function oddEvenChecker(number) {
  if (typeof number === "number") {
    if (number % 2 == 0) {
      //even
      console.log(`The number ${number} is even`);
    } else {
      // odd
      console.log(`The number ${number} is odd`);
    }
  } else {
    alert("Invalid Input");
  }
}

function budgetChecker(budget) {
  const recommendedBudget = 40000;
  if (typeof budget === "number") {
    if (budget > recommendedBudget) {
      console.log("You are over the budget.");
    } else if (budget < recommendedBudget) {
      console.log("You have resources left");
    }
  } else {
    alert("Invalid Input");
  }
}
